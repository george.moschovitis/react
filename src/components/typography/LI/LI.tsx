import * as React from 'react';

/**
 * List item component.
 * @param children {JSX.Element} children of the list item
 * @constructor
 */
export default function LI({ children }) {
  return <li>{children}</li>;
}
