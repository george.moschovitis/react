import * as React from 'react';

export default function UL({ children }) {
  return <ul>{children}</ul>;
}
