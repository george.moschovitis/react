```js
<div>
  <Resource
    key={1}
    icon={
      'https://images.unsplash.com/photo-1558555168-8e12fa8e2870?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60'
    }
    title={'Unsplash random photo'}
    summary={
      'Essendo stat__ sottopost__ a battesimo nella Sua parrocchia, in una data a me non nota ma presumibilmente di poco successiva alla mia nascita, desidero che venga rettificato il dato in suo possesso'
    }
    url={'https://images.unsplash.com/photo-1558555168-8e12fa8e2870'}
    localId={'74'}
  />
</div>
```
