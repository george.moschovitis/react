```js
<div>
  <div style={{ float: 'left' }}>
    <div>Profile</div>
    <div>
      <Avatar size="large">
        <img src="https://picsum.photos/100/100?random" alt="Example avatar" />
      </Avatar>
    </div>
  </div>
  <div style={{ float: 'left', marginLeft: '20px' }}>
    <div>Contributors</div>
    <div>
      <Avatar>
        <img src="https://picsum.photos/100/100?random" alt="Example avatar" />
      </Avatar>
      <Avatar marked>
        <img src="https://picsum.photos/100/100?random" alt="Example avatar" />
      </Avatar>
    </div>
  </div>
  <div style={{ clear: 'both' }}>Blue dot = collection owner</div>
</div>
```
