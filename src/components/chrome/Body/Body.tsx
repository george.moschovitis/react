import { Body as ZenBody } from '@zendeskgarden/react-chrome';

import styled from '../../../themes/styled';

export default styled(ZenBody)`
  flex-grow: 1;
  overflow: auto;
`;
