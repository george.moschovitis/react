import styled from '../../../themes/styled';

export default styled.div`
  padding: 10px;
  overflow: auto;
  display: flex;
  flex: 1;
`;
